class program{
	public static void main(String Shree[]){
	while(true)
		if(false)
			System.out.println("Hello,Core2web");
		System.out.println("true,false");
	}
}
//the error we get from above is
//java:7:unreachable statement 
//the error above comes because 
//have return statement before them  
// have an infinite loop before them  any statement after throwing exception in a try block
//
// MY ASSUMPTION : -
// the block of while contains if and its block of "Hello,Core2Web"
// and as you see while loop is infinite loop and the statement after it WON'T BE REACHED as code/control 
// will not come out of the while loop.
//
// TIP
// always look for 1)is there is empty statement like while(t>45);
// 2)look for braces of statements without which ,which block of statements should be considered
