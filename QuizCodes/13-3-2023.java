class program{
	public static void main(String Shree[]){
	int var=5;
	System.out.println("Inside main");
	while(var-- >3)
	System.out.println("Inside while");
	}
}
//as we know that while loop considers only line below it
//output will be
//
//Inside main
//Inside while
//Inside while
//
//if you declare statement as while(var-- >3);( with semicolon ) 
//the output will be
//
//Inside main
//Inside while
//
//while loop will not consider code below it if while(var-- >3);
