class program{
 public static void main(String Shree[]){
	System.out.println("Before for loop");
  	
	for(int i=0,j=0;i<1;j++){
	System.out.println("Inside For");
}
System.out.println("After for loop");

}

}
//the above code gives output as infinite loop
//as we are not changing the value of i,the loop condition never becomes false resulting in an infinite loop
