class program{

	public static void main(String Shree[]){

	int var1 = 10;
	for(System.out.println(var1); var1 < 15 ;System.out.println(var1),var1++){

	if(var1 == 12){
	System.out.println(var1);
	}
}
}
}
//the first part of for loop : initialization of loop which happens only once hence printing of 10 is done
//second part of for loop :it must be a boolean value as  this expression gives boolean value it is accurate
//third part of for loop:it can contain increment or decrement of variable or statement like given above&it is accurate
//output:10 10 11 12 12 13 14
//first 10 is from initialization 
//second 10 is from third part of for loop after it var1 gets incremented by 1 var1=11
//11 is printed at the third part of for loop for the iteration var=11 and after which var1=12
//for var=12 iteration  if condition satisfies 12 gets printed  and from  third part of for loop 12 gets printed again .Now var=13
//now 13 and 14 gets printed from third part of for loop
