class nano{
	public static void main(String args[]){
	String str1 = "java"; //Ths string will be stored in string constant pool in heap as this is 
					//string literal

	String str2 = new String("java");
	System.out.println(str1 == str2);
}
}
//the answer here is false as for str2 memory is allocated in heap (not in SCP) as by 'new' keyword
//for str1 , the memory is allocated in String Constant Pool

