class nano{
	 public static void main(String args[]){
	String s1 = "this is a string";
	String s2 = "this is a string";
	
	if(System.identityHashCode(s1) == System.identityHashCode(s2)){
	System.out.println("Equal");
	}
	else{
	System.out.println("Not Equal");
	}
}
}
// here the s1 and s2 are string literals ans are allocated in SCP in heap
//as s1 is already present in SCP the reference of s1 or address of s1 is given to s2
//so both of them are considered equal and have same identity hashcode
