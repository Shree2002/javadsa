#include<iostream>
using namespace std;


void solve(int num){

	if(num == 0)
		return;
	
	solve(num--);

	cout<<num<<endl;
}
int main(){
	
	cout<<"Start main"<<endl;
	solve(2);
	cout<<"End main"<<endl;

	return 0;
}
