import java.util.*;

class demo{

	public static void main(String args[]){
	
		Set st = new HashSet();


		st.add(45);
		st.add(78);
		st.add(98);
		st.add(85);

		System.out.println(st);


		ArrayList al = new ArrayList();

		al.add(78);
		al.add(89);
		al.add(54);
		st.addAll(al);	

		System.out.println(st);


		System.out.println(st.contains(45));
		System.out.println(st.isEmpty());


		Iterator itr = st.iterator();

		while(itr.hasNext()){
		
			System.out.println(itr.next());
		}


		//Linked HashSet
		LinkedHashSet lst = new LinkedHashSet();

		lst.add("Shree");
		lst.add("Yash");
		lst.add("Harshal");
		lst.add("Mohit");
		lst.add("Kartik");

		System.out.println(lst);//preserves insertion order

		//TreeSet
		//SortedSet
		SortedSet stt = new TreeSet();


		stt.add("Shree");
		stt.add("Yash");
		stt.add("Harshal");
		stt.add("Mohit");
		stt.add("Kartik");

		System.out.println(stt);//sorted output

		System.out.println(stt.first());
		System.out.println(stt.last());

		System.out.println(stt.headSet("Mohit"));
		System.out.println(stt.tailSet("Harshal"));
		
		//NavigableSet
		NavigableSet nst = new TreeSet();


		nst.add("Shree");
		nst.add("Yash");
		nst.add("Harshal");
		nst.add("Mohit");
		nst.add("Kartik");

		System.out.println(nst.lower("Harshal"));
		System.out.println(nst.higher("Kartik"));
		System.out.println(nst.floor("Harshal"));
		System.out.println(nst.ceiling("Mohit"));

		Iterator ir = nst.iterator();

		while(ir.hasNext()){
		
			System.out.println(ir.next());
		}

	
	}
}
