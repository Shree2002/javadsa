class nano{
	public static void main(String args[]){
	byte b = 50;
	b = b * 2;
	}
}
//here the implicit type conversion of byte to int is done.This is called automatic type conversion.
//Consider the following
//In an expression the precision is required of an intermidiate value will sometimes exceed the range of either operand.E.g.
//class ano{
//	public static void main(String args[]){
//	byte a =40;
//	byte b=50;
//	byte c = 100;
//	c=(a*b); 
//
//}
//}
//In the code above the value of a*b exceeds the range of byte , but this code does not give any error here.
//Implicitly bytes are converted into integers and then evaluated/
//The type conversion sometimes becomes complex as the code at the starting of file looks correct but gives an error of lossy conversion from byte to int.

