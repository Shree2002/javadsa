class demo {
	public static void main(String args[]){
	Parent obj = new Child();
	obj.num(12);
	}

}
class Parent{
	Object num(int one){
	System.out.println("One");
	return 10;
	}
}
class Child extends Parent{
	String num(int two){
	System.out.println("Two");
	return "10";
	}
}
//the call to the method in child class is there
//the above program will never give the error
//as the return type overridden method and overriding method has a parent child relation AND THIS VALID IN JAVA
//THIS CALLED THE COVARIANT RELATION 
//return type of overridden method should be parent and return type overriding method is child class
