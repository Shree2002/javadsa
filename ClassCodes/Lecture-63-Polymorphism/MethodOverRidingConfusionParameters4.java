class client{
 	public static void main(String args[]){
 	demo obj  = new child();
	obj.fun();	
        }	

}

class demo{
	void fun(){
	System.out.println("Fun");
	}
}
class child extends demo{
	void fun(){
	System.out.println("Fun");
	}
}
// the return type of methods i.e. overridden and overriding method should be same if return types are primitive datatypes
// another condition is that datatypes of overridden and overriding methods,if they are classes then they should have relations between them 
// (parent - child) parent-overridden method
// 		    child-overriding method 
