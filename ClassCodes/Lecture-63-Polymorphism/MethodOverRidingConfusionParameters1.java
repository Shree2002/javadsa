
class client{
	public static void main(String args[]){
	Demo obj = new Demo();
	obj.method(12,21);
	obj.method(45,45);
	}
}


class Demo{

	void method(float x,int y){
	System.out.println("First Method");
	}

	void method(int x,float y){
	System.out.println("Second Method");
	}
}
//the reference to the method is ambiguous because the number are compatible with both the methods as size of both the data types is same and there is no data loss
