class client{
	public static void main(String strgs[]){
	Demo obj = new Demo();
	obj.method("Core2Web");
	obj.method(new StringBuffer("Core2Web"));
	obj.method(null);
	}
}

class Demo{
	void method(String str){
	System.out.println("Sring");
	}

	void method(StringBuffer str){
	System.out.println("StringBuffer");
	}
}
// the reference to the 'method' is ambiguous 
// the call is ambiguous is because of null argument 
// as null is compatible with string and stringbuffer 

