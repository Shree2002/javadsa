class client{
	public static void main(String args[]){
	demo obj = new demo();
	obj.fun("Core2Web");
	obj.fun(new StringBuffer("Hare Hare"));
	}
}

class demo{
	void fun(Object obj){
	System.out.println("Object");
	}

	void fun(String srgs){
	System.out.println("String");
	}

}
//the call obj.fun("Core2Web") calls the method fun(String)(compiler checks the method in method table)
//the call to the method fun(new StringBuffer("Hare Hare")) calls the method with object
// as OBJECT IS THE PARENT OF ALL CLASSES DIRECTLY OR INDIRECTLY
// the program run without any error  
