class client {
	public static void main(String args[]){
		demochild obj = new demochild();
		obj.fun();

		demo1 obj1  =  new demochild();
		obj1.fun();

		demo2 obj2 = new demochild();
		obj2.fun();
	}
}

interface demo1{
	default void fun(){
		System.out.println("fun-in-demo1");
	}
}

interface demo2{
	default void fun(){
		System.out.println("fun-in-demo2");
	}
}

class demochild implements demo2,demo1{
	public void fun(){
		System.out.println("fun-in-child");
	}
}
