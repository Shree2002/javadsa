class Demo{

	public static void main(String args[]){
	
		MyThread1 obj1 = new MyThread1();
		MyThread2 obj2 = new MyThread2();

		obj1.start();
		obj2.start();

		System.out.println("In main");
		System.out.println(Thread.currentThread().getName());


	}
}

class MyThread1 extends Thread{

	public void run(){
	
		System.out.println("In run thread1");
		System.out.println(Thread.currentThread().getName());	
	}
}

class MyThread2 extends Thread{

	public void run(){
	
		System.out.println("In run thread2");
		System.out.println(Thread.currentThread().getName());
	}
}


//number of threads is equal to the number of stacks in jvm
