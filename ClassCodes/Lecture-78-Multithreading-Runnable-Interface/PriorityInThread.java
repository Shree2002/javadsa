class MyThread extends Thread{
	
	public void run(){
	
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getPriority());
	
	}

}
class Demo{

	public static void main(String args[]){
	
		Thread t = new Thread();
		System.out.println(t.getName());
		System.out.println(t);// Thread[main,5,main] means [groupname,priority,threadname]
	
		t.setPriority(7);

		MyThread obj1 = new MyThread();
		obj1.start();

		t.setPriority(7);//set priority to 7 and threads below it will have this priority as they are born from main thread

		MyThread obj2 = new MyThread();
		obj2.start();


	

	}

}


