class Program2{
	public static void main(String[]args){
	float temp=56.6f;                //here float values should conatain 'f' to specify that the number is float 	
					//otherwise the value will be considered as DOUBLE as "incompatible types lossy conversion from float to double"
	//The below approach uses a && operator which can be avoided 
     	//if(temp>98.6f){

	 //System.out.println("High");
	//}
	//else if(temp>=98.0f&&temp<98.0){
	//System.out.println("Normal");
	//}
	//else{
	//	System.out.println("Low");
	//}

	if(temp<98.0){
	System.out.println("Low");
	}
	else if(temp>98.6f){
	System.out.println("High");
	}
	else{
	System.out.println("Normal");
	}

	}

};

