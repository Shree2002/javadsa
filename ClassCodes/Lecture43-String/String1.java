class nano{
	public static void main(String args[]){
	String str1 = "Shashi";
	String str2 = "Bagal";
	
	String str3 = str1 + str2;// here both sides of plus have strings and this calls to the append method of 
					//string builder class(you can see it in bytecode)
	String str4 = str1.concat(str2);

	System.out.println(str3);
	System.out.println(str4);
}

}
