class Outer{
	
	int x = 10;

	class Inner{
	
		int y=20;
		//static is not allowed here bacause the statement Outer.Inner.z cannot be done as "Inner" is not the 
		//static class here,even (outer's object).Inner.z is also not possible for same reason
	}


}
//this$0 here represent the reference to the outer class where Inner class is located
//0 is the level of class here
//constructor contains the hidden this parameter and Outer class reference
//hence to fill the first this object creation is must and hence the static variavbles are not allowed but
//if we put the final keyword before static variable then ist is allowed
//
//there is no paren chils relationship between the inner and outer class here
