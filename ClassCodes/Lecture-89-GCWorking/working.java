import java.util.*;

class demo{

	public static void main(String args[]){
		
		demo1 obj1 = new demo1("Shree");
		demo1 obj2 = new demo1("Yash");
		demo1 obj3 = new demo1("Kartik");

		System.out.println(obj1+" "+obj2+" "+obj3);
		obj1=null;
		System.gc();
		System.out.println(" ");

	}

}

class demo1{

	String str=null;

	demo1(String str){
	
		this.str=str;
	}

	public String toString(){

		return str;
	}

	public void finalize(){
	
		System.out.println("Notify");
	}
}
