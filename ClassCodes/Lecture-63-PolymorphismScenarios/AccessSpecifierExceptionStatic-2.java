class client{
	public static void main(String args[]){
	parent obj = new child();
	obj.fun();
	}
}
class parent{
	static void fun(){
	System.out.println("Fun");
	}
}
class child extends parent{
	void fun(){
	System.out.println("FUN");
	}
}
//static cannot be overridden as static modifier is not the part od overriding
//vice versa is also not possible here
