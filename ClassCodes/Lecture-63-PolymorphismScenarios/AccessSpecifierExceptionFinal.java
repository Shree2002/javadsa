class client{
	 public static void main(String args[]){
	 parent obj = new child();
	 obj.fun();
	 }

}
class parent {
	final void fun(){
	System.out.println("Final");
	}
}
class child extends parent{
	void fun(){
	System.out.println("Fun");
	}
}
//final cannot be overridden at any cost 
