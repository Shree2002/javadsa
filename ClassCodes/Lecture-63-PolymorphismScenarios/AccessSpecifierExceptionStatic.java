class client{
	public static void main(String args[]){
	parent obj = new parent();
	obj.fun();

	child obj1 = new child();
	obj1.fun();
		
	parent obj2 = new child();
	obj2.fun();
	}
}
class parent{
	static void fun(){
	System.out.println("Fun");
	}
}
class child extends parent{
	static void fun(){
	System.out.println("fun");
	}
}
//static modifier IS NOT INCLUDED IN THE METHOD OVERRIDING .
//here both the methods are treated as different methods in their respective classes
//hence at the time object creation the method from the redrenced class (on left hand side of =) is called or invoked 
