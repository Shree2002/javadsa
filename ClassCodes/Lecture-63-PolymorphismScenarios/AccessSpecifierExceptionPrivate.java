class client{
	public static void main(String args[]){
	parent obj = new child();
	obj.fun();	
	}
}
class parent{
	private void fun(){
	System.out.println("private");
	}
}
class child extends parent{
	void fun(){
	System.out.println("Fun");
	}
}
//here even the overridden method has access specifier private and the overriding method has access apecifier default
//even the scope in incresing this is not valid as privates are not accessible in other classes than itself
//ot can aven seen in byte code that privates are not there
//st compile time the compiler binds or finds the method in parents method table and if same method is present in the child then only it excutes the childs method at runtime
