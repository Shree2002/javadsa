class client{
	public static void main(String args[]){
	child obj = new child();
	obj.fun();

	}
}
class parent{
	public void fun(){
	System.out.println("Fun");
	}
}
class child extends parent{
	void fun(){
	System.out.println("No public fun");
	}
}
//the overriden method has public access specifier and overrding method has default access specifier 
//scope of method becomes smaller hence the error
