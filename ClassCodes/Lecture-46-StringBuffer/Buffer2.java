class nano{

	public static void main(String args[]){
	String str1 = "Shashi";
	String str2 = new String("Bagal");
	StringBuffer str3 = new StringBuffer("Core2Web");
	System.out.println(str3.capacity());
	
	String str4 = str1.append(str3);
	System.out.println(str4);
	
}

}
//here error is cannot find symbol becouse string (str1) ditn have any method named as append string class have concat method 
//for that
//the concat method accepts the string as parameter(it must be string strictly)
//concat emthod return the updated string and it must be stored in some string variable
//otherwise the change will not be reflected(the change is not done at str1 if str1.concat(ANY STRING) is used)
