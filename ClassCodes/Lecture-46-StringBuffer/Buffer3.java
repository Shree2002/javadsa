class nano{
	public static void main(String args[]){

	String str1= "Shashi";
	String str2 = new String("Bagal");
	StringBuffer str3 = new StringBuffer("Core2Web");
	StringBuffer str4 = str3.append(str1);//return type of this append is StringBuffer
						//and even though we have not created the object here
						//it will internally go for new StringBuffer("Core2WebShashi");
	System.out.println(str4);

}

}
//code will not give error at str4
