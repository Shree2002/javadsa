import java.util.*;

class Demo{

	public static void main(String args[]){
	
		TreeSet t = new TreeSet();

		t.add(new Movie("OMG2",89.9f));
		t.add(new Movie("Jailer",90.9f));
		t.add(new Movie("Gadar2",52.9f));

		System.out.println(t);

	}

}
//internal call goes to compareTo method as TressSet sorts the data
//and as we implement the comparable interface we must override its method
class Movie implements Comparable{

	String name = null;
	float coll = 0.0f;

	Movie(String name,float coll){
	
		this.name = name;
		this.coll = coll;
		System.out.println("This is coll"+coll);
	}

	public String toString(){
	
		return "{ "+name+" : "+coll+" }";
	}

	public int compareTo(Object obj){

		return name.compareTo(((Movie)obj).name);
	}
}
