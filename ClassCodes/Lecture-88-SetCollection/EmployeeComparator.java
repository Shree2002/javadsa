import java.util.*;

class Demo{

	public static void main(String srgs[]){
	
		ArrayList al = new ArrayList();

		al.add(new Employee("Harshal",90.8F));
		al.add(new Employee("Kartik",89.6F));
		al.add(new Employee("Mohit",91.8F));
		al.add(new Employee("Kartikeyan",88.6F));

		System.out.println(al);

		Collections.sort(al,new SortByName());
		
		System.out.println(al);

	}
}

class Employee{

	String name = null;
	float sal = 0.0f;

	Employee(String name,float sal){
	
		this.name = name;
		this.sal = sal;
	}

	public String toString(){
	
		return name+" "+sal;
	}
}

class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((((Employee)obj1).name).compareTo(((Employee)obj2).name));
	}
}

class SortBySal implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return (int)((((Employee)obj1).sal)-(((Employee)obj2).sal));
	}

}
//the comparable is not implemented here as compareTo is not called internally
