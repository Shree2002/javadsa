class nano{
	public static void main(String args[]){
	Parent obj = new Child();// a valid statement (parent reference and child object is valid in java)
	obj.info();//a valid statement
	
	}

}
class Parent{
	Parent(){   //Parent this
	System.out.println("The constructor of parent class");
	}
	void info(){ //Parent this
	System.out.println("The Gold , The Flat , The Car");
	
	}
	
}
class Child extends Parent{
	Child(){//Child this
	System.out.println("The constructors of Child class");
	}

}
