import java.util.*;

class SetDemo{

	public static void main(String args[]){
	
		TreeSet t = new TreeSet();

		t.add(new MyClass("Kotgiri"));
		t.add(new MyClass("Shree"));
		t.add(new MyClass("Laxmikant"));
	
		System.out.println(t);

	}
}

class MyClass implements Comparable{

	String name = null;

	MyClass(String name){
	
		this.name = name;
	}

	public int compareTo(MyClass ans){
	
		return (ans.name).compareTo(this.name);
		
	}

}
