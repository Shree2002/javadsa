class ThreadDemo{

	public static void main(String args[]){
	
		myThread obj1 = new myThread("1-Thread");
		myThread obj2 = new myThread("2-Thread");
		myThread obj3 = new myThread("3-Thread");

		myThread obj = new myThread();

		obj1.start();
		obj2.start();
		obj3.start();
		obj.start();

	
	}

}
class myThread extends Thread{

	myThread(){
	
		super();
	}

	myThread(String str){
	
		super(str);
	
	}
	public void run(){
	
		System.out.println(getName());
	}

}
