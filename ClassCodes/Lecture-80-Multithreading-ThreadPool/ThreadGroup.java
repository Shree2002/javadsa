class ThreadDemo{

	public static void main(String args[]){
	
		ThreadGroup group = new ThreadGroup("Core2Web");
		
		myThread obj1 = new myThread(group,"C");
		myThread obj2 = new myThread(group,"Java");
		myThread obj3 = new myThread(group,"Python");
		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup childgroup = new ThreadGroup(group,"Incubator");
		
		myThread obj4 = new myThread(childgroup,"Flutter");
		myThread obj5 = new myThread(childgroup,"ReactJS");
		myThread obj6 = new myThread(childgroup,"Springboot");
		obj4.start();
		obj5.start();
		obj6.start();


	}

}

class myThread extends Thread{

	myThread(ThreadGroup tg , String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println(getName());
	}

}
