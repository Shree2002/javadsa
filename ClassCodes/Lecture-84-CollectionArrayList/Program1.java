import java.util.*;

class ArrayListDemo extends ArrayList{

	public static void main(String args[]){
	
		ArrayListDemo obj = new ArrayListDemo();

		//add(Object)
		obj.add(10);
		obj.add(20.4f);
		obj.add("Shree");
		obj.add(20.4f);
		obj.add(78.45d);
		System.out.println(obj);

		//int size()
		System.out.println(obj.size());

		//boolean contains(Object)
		System.out.println(obj.contains(20.4f));
		System.out.println(obj.contains(40));
	
		//int indexOf(Object)
		System.out.println(obj.indexOf("Shree"));

		//int lastIndexOf(Object)
		System.out.println(obj.lastIndexOf("20.4f"));

		//E get(int)-element at (int)th position
		System.out.println(obj.get(2));

		//E set(int,E)
		System.out.println(obj.set(2,22));
		System.out.println(obj);

		//E set(int,E) - removes and returns the element at (int)th position
		System.out.println(obj.remove(2));
		System.out.println(obj);

		//add(int , E)
		obj.add(3,"Kal");
		System.out.println(obj);

		//protected removeRange(int,int) - as it is protected , ArrayList id extended
		//ans object of ArrayListDemo class is used inorder to use this method
		obj.removeRange(2,4);
		System.out.println(obj);

		//addAll(Collection)
		ArrayList<String> anno = new ArrayList<String>();
		anno.add("Like1");
		anno.add("Like2");
		anno.add("Like3");
		obj.addAll(1,anno);

		//Object[] toArray
		Object arr[] = obj.toArray();
		System.out.println(arr);

		for(Object data : arr){
		
			System.out.println(data);
		}



	}

}
