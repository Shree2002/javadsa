import java.util.*;

class QueueDemo{

	public static void main(String args[]){
	
		Queue q = new PriorityQueue();
		
		q.offer(10);
		q.offer(20);
		q.offer(50);
		q.offer(60);
		q.offer(40);

		System.out.println(q);

		//PriorityQueue
		
		System.out.println("PriorityQueue Implmentation");

		ArrayList al = new ArrayList();

		al.add("Kartik");
		al.add("Yash");

		PriorityQueue q1 = new PriorityQueue(al);

		q1.offer("Kanha");
		q1.offer("Ashish");
		q1.offer("Rahul");
		q1.offer("Rajesh");
		q1.offer("Badhe");

		System.out.println(q1);

		System.out.println(q1.peek());
		System.out.println(q1.remove());
		System.out.println(q1);


		//Queue by linked list

		Queue q2 = new LinkedList();
		q2.offer(10);
		q2.offer(20);
		q2.offer(50);
		q2.offer(30);
		q2.offer(40);

		System.out.println("New Queue "+q2);

		System.out.println("Poll "+q2.poll());
		System.out.println("Remove "+q2.remove());

		System.out.println(q2);

		System.out.println("Peek "+q2.peek());
		System.out.println("Element"+q2.element());

		System.out.println(q2);

		//Deque by ArrayDeque
		System.out.println("Deque by ArrayDeque");
		Deque d = new ArrayDeque();

		d.offer(10);
		d.offer(30);

		System.out.println(d);

		d.addFirst(25);
		d.addLast(35);

		System.out.println(d);

		d.removeFirst();
		d.removeLast();

		System.out.println(d);

		Iterator itr = d.iterator();
		while(itr.hasNext()){
		
			System.out.println("Itr : "+itr.next());
		}

		Iterator ditr = d.descendingIterator();
		while(ditr.hasNext()){
		
			System.out.println("Ditr : "+ditr.next());
		}



	}
}
