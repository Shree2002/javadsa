
class Demo{

	public static void main(String args[]){
	
		CustomThread t1 = new CustomThread();
		t1.start();

		CustomThread t2 = new CustomThread();
		t2.start();

		t2.customNamePriority("Hello",7);
	}
}
class CustomThread extends Thread{

	public void run(){
	
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getPriority());
	}

	void customNamePriority(String str,int prio){
		Thread.currentThread().setName(str);
		Thread.currentThread().setPriority(prio);

		System.out.println("After changing priority");
	
		System.out.println(Thread.currentThread().getName());	
		System.out.println(Thread.currentThread().getPriority());
}
}
