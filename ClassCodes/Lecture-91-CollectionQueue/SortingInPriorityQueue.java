import java.util.*;

class demo{

	public static void main(String args[]){
	
		PriorityQueue obj = new PriorityQueue();

		obj.offer(new Project("Webapp",30,5));
		obj.offer(new Project("Desktop App",12,8));
		obj.offer(new Project("Mobile App",4,25));

		System.out.println(obj);
	}
}

class Project implements Comparable{

	String name = null;
	int duration = 0;
	int teamsize = 0;

	Project(String name,int duration,int teamsize){
	
		this.name=name;
		this.duration=duration;
		this.teamsize=teamsize;
	}

	public String toString(){
	
		return name;
	}

		public int compareTo(Object obj){
	
		System.out.println("Hello");
		return (this.name).compareTo(((Project)obj).name);
	}
}
