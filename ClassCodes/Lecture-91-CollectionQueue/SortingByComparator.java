import java.util.*;

class demo{

	public static void main(String args[]){
	
		PriorityQueue pq = new PriorityQueue(new SortByduration());

		pq.offer(new Project("WebApp",50));
		pq.offer(new Project("Desktop App",20));
		pq.offer(new Project("Mobile App",2));

		System.out.println(pq);
	}
}

class Project{

	String name=null;
	int duration=0;

	Project(String name,int duration){
	
		this.name=name;
		this.duration=duration;
	}

	public String toString(){
	
		return name;
	}
}

class SortByduration implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Project)obj1).duration - ((Project)obj2).duration;
	}
}
