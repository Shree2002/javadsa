class demo{


	void solve(int num){
	
		if(num==0)
			return;

		solve(--num);

		System.out.println(num);
	
	}

	public static void main(String args[]){
		
		demo d = new demo();
		d.solve(2);

	}
}
//non tailed recursion as last call is not to function itself
//values passed are local to that stack frame in java
//
//1st iteration
//2 is passed and in solve function ,value is decremented in recursive call 1 value is printed and further iterations are carried in same method 
