class main{


	int sumD(int num){
	
		if(num <= 0)
			return 0;

		return num%10 + sumD(num/10);
	}

	public static void main(String args[]){
	
		main d = new main();
		System.out.println(d.sumD(1234));
	}
}
