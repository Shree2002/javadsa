import java.util.*;


class demo{
	public static void main(String args[]){
	
		ArrayList al = new ArrayList();

		al.add(new employee("Kartik",40));
		al.add(new employee("Harshad",30));
		al.add(new employee("Yash",20));
		al.add(new employee("Shree",10));

		System.out.println(al);

		Collections.sort(al,(Object obj1,Object obj2)->{
		
			return ((employee)obj1).name.compareTo(((employee)obj2).name);
		});
	}

}

class employee{

	String name=null;
	int id = 0;

	employee(String name,int id){
		this.name=name;
		this.id=id;
	}

	public String toString(){
	
		return name;
	}
}
