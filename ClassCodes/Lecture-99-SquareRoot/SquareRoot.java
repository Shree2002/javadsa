class SquareRoot{

	//Optimized Approach
	
	static int findSqrt(int n){

		int start=1;
		int end=n;
		int mid=0;
		int ans=0;
		while(start<=end){
			 mid=(start+end)/2;

			 if(mid*mid==n){
			 	return mid;
			 }
			 else if(mid*mid>n){
			 	end=mid-1;
			 }
			 else{
			 	start=mid+1;
				ans=mid;
			 }
		}

		return ans;
	}

	public static void main(String args[]){
	
		System.out.println(findSqrt(50));
	}

}
