//Cases having same values are not allowed in switch

//The code below gives error as duplicate label is not allowed(Compile time error)

class codes{

	public static void main(String Shree[]){

		int x = 5;
		switch(x){
		case 1:
			System.out.println("One");
			break;
		case 2:
			System.out.println("Two");
			break;
		case 2:
			System.out.println("Two Two");
			break;
		default:
			System.out.println("No match");

		}


}



}
