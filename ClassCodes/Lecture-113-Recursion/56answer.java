class demo{

	static int call = 0;

	static int four(int num){

		call++;
	
		if(num<=0)
			return 2;

		int ans1 = four(num-1)+four(num-2);
		int ans2 = four(num-2)+four(num-1);

		return ans1+ans2;
	}

	public static void main(String args[]){
	
		System.out.println(four(3));
		System.out.println("Call : " + call);
		
	}
}
