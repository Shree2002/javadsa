class demo{

	static int ans=5;

	static void binary(int arr[],int start,int end,int num){
	
		if(start > end){
			return;
		}
		else{
		
			int mid = (start+end)/2;

			if(arr[mid] == num){
				ans = mid;		
			}

			if(arr[mid] > num)
				 binary(arr,mid+1,end,num);

			else
				 binary(arr,start,mid-1,num);

		}
	}

	public static void main(String args[]){
		
		int arr[] = {4,7,8,9,14,16,45,89,99};

		binary(arr,0,arr.length-1,16);

		System.out.println(ans);

	}
}

