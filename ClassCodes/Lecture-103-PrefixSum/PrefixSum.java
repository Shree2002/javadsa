import java.util.*;

class Demo{

	public static void main(String args[]){
		
		int arr[] = {-3,6,2,4,5,2,8,-9,3,1};
		
		Scanner sc = new Scanner(System.in);

		System.out.println("The test cases ");
		int itr = sc.nextInt();

		//in place conversion of given array 

		for(int i=1;i<10;i++){
		
			arr[i] = arr[i]+arr[i-1];
			System.out.println(i+1+"th value is "+arr[i]);
		}

	
		
		int sum=0;

		for(int i=0;i<itr;i++){
			
			int s = sc.nextInt();
			int e = sc.nextInt();
				
			System.out.println(arr[s-1]);
			System.out.println(arr[e]);

			if(s == 0)
				sum = arr[e];
			else
				sum = arr[e]-arr[s-1];

			System.out.println(sum);
		
		}

		
	
	}
}
