class nano{
	static void fun(int arr[]){
	System.out.println(arr);//address of array
	for(int x:arr){
	System.out.println(x);
	}
	for(int i=0;i<arr.length;i++){
	arr[i] = arr[i]+10;
	}
	}
	public static void main(String Shree[]){
	int arr[] = {10,20,30};
	fun(arr);	//address is passed as an argument
	System.out.println(arr);//address of array
	for(int x:arr){
	System.out.println(x);
	}

	}

}
// the address is passed as argument when passing a array as an argument
//so changes made in method fun will reflected in original array 
