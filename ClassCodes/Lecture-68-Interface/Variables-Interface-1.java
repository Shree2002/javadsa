class client{
	public static void main(String args[]){
	
		Child obj = new Child();
		obj.fun();
		
	}
}

interface A{
	int x = 10;//public static final x
}

interface B{
	int x = 20;//public static final x
}

class Child implements A,B{
	
	int x = 30;

	void fun(){
		System.out.println(x);
		System.out.println(A.x);
		System.out.println(B.x);
	}

}
//the first x will give the value near to method 
//other values will be of A,B interfaces respectively
//there is a protocol that variables inside interfaces shoul be called with their reference
//to acoid the ambiguity 
