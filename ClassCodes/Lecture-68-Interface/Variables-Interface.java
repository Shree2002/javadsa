class Client{
	public static void main(String args[]){
	
		Child obj = new Child();

		obj.fun();
	}

}

interface Demo{
	int x = 10;//public static final x
		   //its value is assigned when it is called
	void fun(); //public abstract fun
}

class Child implements Demo{
	public void fun(){

		System.out.println("Fun in Child");

	}
}
//variables declared inside the interface have public static final prefix
//when the variables inside interface if they are called inside the method in the class where the 
//the method is implemented , the variable is initialised at the time of call
//if variable is called then it will be initialised independent of whether it is called previously or not
