The time complexity deals with the amount of time required for function to perform designated task.
The time complexity ie denoted by mostly three asymptotic notations -(1)Omega-for best case
(2)Theta-Average case (3)Big O - Worst case

We use and companies ask only Big O time complexity.

For calculation of time complexity from code we use following steps:
1) One must calculate the number of iterations
2) Ignore or cut the lower ordered terms
3) Remove the constant coefficient

Here the term with most significance is chosen because, lower ordered turn out to be less in time as compared to high ordered terms
Ex.N^2 + 2N

here 2N will be ignored as the input increases the ,the value N^2 increases so much as compared to 2N,hence this 2N term can be ignored and coefficient is also ignored.

Types of time complexities:-
1)O(1) - Constant time complexity 
2)O(logN) - Logarithmic time complexity
3)O(root of N) - Root time complexity
4)O(N) - Linear time complexity
5)O(NlogN) - Linear logarithmic time complexity
6)O(N^2) - Quadratic time complexity
7)O(N^3) - Cubic time complexity
8)O(N!) - Factorial time complexity

N raised to anything is considered as polynomial time complexity
