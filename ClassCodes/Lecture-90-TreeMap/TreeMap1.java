import java.util.*;

//implementation of comparable is there is therenis sorting by user created objects

class TreeMapDemo{

	public static void main(String args[]){
	
		TreeMap tp = new TreeMap();

	tp.put(new Platform("OnlyFans",2016)," OnlyFans");
	tp.put(new Platform("Youtube",2005)," Google");
	tp.put(new Platform("Facebook",2004)," Meta");

	System.out.println(tp);

	}
}

class Platform implements Comparable{

	String Pname=null;
	int year = 0;

	Platform(String Pname,int year){
	
		this.Pname = Pname;
		this.year=year;
	}

	public String toString(){
	
		return Pname+" : "+year+" ";
	}

	public int compareTo(Object obj){
	
		return this.year - ((Platform)obj).year;
	}

}


