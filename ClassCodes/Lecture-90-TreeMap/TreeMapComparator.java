import java.util.*;

class Demo{

	public static void main(String args[]){
	
		//TreeMap tp = new TreeMap(new SortByName());
		TreeMap tp = new TreeMap(new SortByYear());

		tp.put(new Platform("OnlyFans",2016),"OnlyFans");
		tp.put(new Platform("Youtube",2005),"Google");
		tp.put(new Platform("Instagram",2013),"Meta");
		tp.put(new Platform("ChatGPT",2022),"OpenAI");
		tp.put(new Platform("Twitter",2006),"X");
		
		System.out.println(tp);

	}
}


class Platform{

	String name = null;
	int year = 0;

	Platform(String name,int year){
	
		this.name = name;
		this.year = year;
	}

	public String toString(){
	
		return name+" : "+year+" ";
	}

}

class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Platform)obj1).name.compareTo(((Platform)obj2).name);
	}
}

class SortByYear implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Platform)obj1).year - ((Platform)obj2).year;
	}
}


