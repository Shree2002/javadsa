import java.util.*;
import java.io.*;

class Demo{

	public static void main(String args[])throws Exception{
	
		Properties obj = new Properties();
		FileInputStream ip = new FileInputStream("friends.properties");

		obj.load(ip);

		String name = obj.getProperty("Shree");
		System.out.println(name);

		System.out.println(obj.getProperty("Harshal"));

		obj.setProperty("Sneha","EY");

		FileOutputStream op = new FileOutputStream("friends.properties");

		obj.store(op,"Updated By Shree");


	}
}
