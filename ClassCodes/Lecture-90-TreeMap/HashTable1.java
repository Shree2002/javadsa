import java.util.*;

class Demo{

	public static void main(String args[]){
	
		Hashtable ht = new Hashtable();

		ht.put(10,"Sachin");
		ht.put(1,"JLRahul");
		ht.put(45,"Rohit");
		ht.put(18,"Virat");
		ht.put(7,"MSD");

		System.out.println(ht);

		Enumeration enu = ht.keys();
		
		while(enu.hasMoreElements()){
		
			System.out.println(enu.nextElement());
		}


		Enumeration enu1 = ht.elements();
	
		while(enu1.hasMoreElements()){
		
			System.out.println(enu1.nextElement());
		}


	}
}
