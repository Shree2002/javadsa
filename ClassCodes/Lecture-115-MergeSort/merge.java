class demo{

	static void mergeArrays(int start,int mid,int end,int[] arr){

		int n1 = mid-start+1;
		int n2 = end-mid;

		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		for(int i=0;i<arr1.length;i++){
		
			arr1[i] = arr[start+i];
		}

		for(int i=0;i<arr2.length;i++){
			arr2[i] = arr[mid+1+i];
		}

		int i=0,j=0,k=start;

		while(i < n1 && j < n2){
		
			if(arr1[i] < arr2[j]){
			
				arr[k]=arr1[i];
				i++;
			}
			else {
			
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < n1){
		
			arr[k] = arr1[i];
			i++;
			k++;
		}

		while(j < n2){
		
			arr[k]  =  arr2[j];
			j++;
			k++;
		}

			
	}

	static void mergeSort(int[] arr,int start,int end){
		
		if(start<end){
		
			int mid=start+(end-start)/2;

			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);

			mergeArrays(start,mid,end,arr);

		}
		
	}

	public static void main(String args[]){
	
		int arr[]={9,1,2,3,4,5,7};

		mergeSort(arr,0,arr.length-1);

		for(int i=0;i<arr.length;i++){
		
			System.out.println(arr[i]);
		}

	}
}
