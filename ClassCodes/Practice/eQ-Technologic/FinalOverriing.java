class C{

	public static void main(String args[]){
	
		B b = new B();
		b.show();

	}
}

class A{
	
	public final void show(){
	
		System.out.println("In show parent");

	} 
}
class B extends A {

	public void show(){
	
		System.out.println("In show child");
	}
}

