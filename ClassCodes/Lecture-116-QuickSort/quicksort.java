import java.util.*;

class QuickSort{
	
	int partition(int arr[],int start,int end){
		
		int pivot = arr[end];
		int i = start-1;

		for(int j=start;j<end;j++){
		
			if(arr[j]<pivot){
			
				i++;
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}

		}

		i++;
		int temp = arr[i];
		arr[i] = arr[end];
		arr[end] = temp;

		return i;
	}

	void quickSort(int arr[],int start,int end){
	
		if(start < end){
			
			int index = partition(arr,start,end);

			quickSort(arr,start,index-1);
			quickSort(arr,index+1,end);

		}
	}

	public static void main(String args[]){
	
		QuickSort sort = new QuickSort();

		int arr[] = new int[]{5,8,4,1,2,3};

		sort.quickSort(arr,0,5);

		System.out.println(Arrays.toString(arr));

	}

}
