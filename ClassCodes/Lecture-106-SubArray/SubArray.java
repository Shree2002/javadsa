//return minimum length of subarray containing max and min elements in an array

class m{

	public static void main(String args[]){
	
		int arr[] = new int[]{1,2,3,1,3,4,6,4,6,3};

		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i]>max){
			
				max = arr[i];
			}

			if(arr[i]<min){
			
				min = arr[i];
			}
		}

		int len = arr.length;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] == max){
			
				for(int j=i+1;j<arr.length;j++){
				
					if(arr[j] == min){
					
						if(len > j-i+1){
						
							len=j-i+1;
						}
					}
				}
			}
			else if(arr[i] == min){
			
				for(int j=i+1;j<arr.length;j++){
				
					if(arr[j] == max){
					
						if(len > j-i+1){
						
							len = j-i+1;
						}
					}
				}
				
			}
		}


		System.out.println(len);
	}
}
