class Client{
	public static void main(String args[]){
	Child obj = new Child();	
	obj.method();
	}
}

abstract class Parent{

	void fun(){
	System.out.println("Fun");
	}

	abstract void method();
}

class Child extends Parent{
	void method(){
	System.out.println("Child-method");
	}
}
 
