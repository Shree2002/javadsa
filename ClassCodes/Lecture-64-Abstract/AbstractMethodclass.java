abstract class Parent{
	void fun(){
	System.out.println("Parent-fun");
	}

	abstract void method();
}
class client{
	public static void main(String Shree[]){
	Parent obj = new Parent();
	}

}
//abstract class cannot be instantiated  as the implementation of the abstract methods is incomplete
//the implentation of the abstract class should be done in the child class of the abstract class
//the abstract method does have a constructor in order to access the methods of parent class of abstract class

