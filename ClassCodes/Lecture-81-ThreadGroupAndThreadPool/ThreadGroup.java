class ThreadDemo{

	public static void main(String args[]){
	
		ThreadGroup pThread = new ThreadGroup("India");

		myThread t1 = new myThread(pThread,"Maharashtra");
		myThread t2 =  new myThread(pThread,"Goa");

		ThreadGroup cThread = new ThreadGroup(pThread,"Pakistan");

		myThread t3 = new myThread(cThread,"Karachi");
		myThread t4 = new myThread(cThread,"Lahore");

		ThreadGroup cThread1 = new ThreadGroup(pThread,"Bangladesh");

		myThread t5 = new myThread(cThread1,"Dhaka");
		myThread t6 = new myThread(cThread1,"Mirpur");

		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();

		cThread.interrupt();


		try{

		Thread.sleep(5000);

		}
		catch(InterruptedException ie){
		
		}

		System.out.println(pThread.activeCount());//here the active count of threads will become 0 as all threads will complete their execution till here 
		System.out.println(pThread.activeGroupCount());//the count of child thread groups of pThread will be 2 as there are two children of pThread is there and no termination of child threadgroups 

	}

}

class myThread extends Thread{

	myThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}


	public void run(){
	
		System.out.println(Thread.currentThread());

		try{
		
			Thread.sleep(5000);
		}
		catch(InterruptedException ie){
		
			System.out.println(ie.toString());
		}
	
	}

}

