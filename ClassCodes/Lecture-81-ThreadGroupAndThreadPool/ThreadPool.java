import java.util.concurrent.*;

class ThreadPoolDemo{

	public static void main(String args[]){
	
		ExecutorService ser = Executors.newFixedThreadPool(3);

		for(int i=1;i<=6;i++){
		
			myThread obj = new myThread(i);
			ser.execute(obj);
		}
		
		ser.shutdown();

	}

}


class myThread implements Runnable{

	int num;

	myThread(int num){
		this.num = num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread() + " Start Thread : " + num);
		dailyTasks();
		System.out.println(Thread.currentThread() + " End Thread : " + num);

	}

	void dailyTasks(){
	
		try{
		
			Thread.sleep(5000);
		}
		catch(InterruptedException ie){
		
		}
	}

}
