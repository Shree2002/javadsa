class ThreadDemo{

	public static void main(String args[]){
	
		ThreadGroup pThread = new ThreadGroup("India");

		Thread t1 = new Thread(pThread,new myThread(),"Maharashtra");
		Thread t2 = new Thread(pThread,new myThread(),"Goa");

		ThreadGroup cThread=new ThreadGroup("Pakistan");

		Thread t3 = new Thread(cThread,new myThread(),"Karachi");
		Thread t4 = new Thread(cThread,new myThread(),"Lahore");

		t1.start();
		t2.start();
		t3.start();
		t4.start();

	}
}

class myThread implements Runnable{

	public void run(){
	
		System.out.println(Thread.currentThread());

		try{
		
			Thread.sleep(5000);
		
		}
		catch(InterruptedException ie){
		}
	}
}
