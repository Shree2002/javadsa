class client{
	public static void main(String args[]){
	Demo obj = new Demochild1();
	obj.gun();
	obj.fun();
	}
}

interface Demo{
	void fun();
	void gun();
}
abstract class DemoChild implements Demo{
	public void gun(){
	System.out.println("Gun");
	}
}
class Demochild1 extends DemoChild{
	public void fun(){
	System.out.println("fun");
	}
}
//Demochild inherits all the methods from Demo and only gu is implemented in this class hence delared 
//abstract
//Demochild1 has method implemnted from abstract class and it own implemented the fun method 
