class FinallyDemo{
	
	public static void main(String args[]){
		
		try{
			System.out.println(10/0);
		}
		catch(ArithmeticException obj){
			
			System.out.println("Arithmatic");
		
		}
		finally{
			System.out.println("Finally");
		}
	
	}
}
//no matter what, finally will execute.It will not excecute only if System.close() or system crash
