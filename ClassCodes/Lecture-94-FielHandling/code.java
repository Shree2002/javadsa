import java.io.*;

class demo{

	public static void main(String args[])throws IOException{
	
		File fobj = new File("Incubator.txt");
		//the file is not created here the pointer to the file is created here 
		fobj.createNewFile();//here new file will be created with specified name and if already present then no new file will be created 
	
		File fobj2 = new File("Shashi");
 		fobj2.mkdir();		

	       File fobj1 = new File("Shashi","Incubator1");//the directory specified in first parameter is should be present 
	

       	//here if extension will be provided then for second parameter file will be created otherwise folder will be created
			      fobj1.createNewFile(); 
		


	}
}
