class client{
	public static void main(String args[]){
	Demo1 obj = new DemoChild();//the left side should be interface containing the method i.e.
				    //can be Demo1,Demo2
				    //the right side should be a concrete class
	obj.fun();
	}


}

interface Demo1{
	void fun();
}
interface Demo2{
	void fun();
}

class DemoChild implements Demo1,Demo2{
	public void fun(){
	System.out.println("Fun");
	}

}



//as fun dont have body in both the interfaces only one declaration can be enough for implementation
//hence the multiple inheritance problem solved
