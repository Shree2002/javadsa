//if the code does not contain break statements in between cases then ,when the 
//condition satisfies,the bodies of the cases below the satisfied case are considered
//and executed even the body of DEFAULT statement .

//break after the default is necessary or not depends upon the position of the default inside the switch
//default inside the switch's cases have the LEAST PRIORITY IRRESPECTIVE OF IT'S POSITION


class codes{
public static void main(String Shree[]){
	int x = 2;
	switch(x){
	case 1:
		System.out.println("1");
		break;
	case 2:
		System.out.println("2");
		break;
	case 3:
		System.out.println("3");
		break;
	default:
		System.out.println("No match");
		break;

	}


}
}
