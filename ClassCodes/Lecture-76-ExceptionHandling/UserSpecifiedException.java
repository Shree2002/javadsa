import java.util.*;
class Demo{


	public static void main(String args[]){
	
		int data = new Scanner(System.in).nextInt();

		if(data <= 0){
			
			throw new DataUnderflowException("dataunderflow");
		
		}

		if(data >=100){
		
			throw new DataOverflowException("dataoverflow");
		}

	
	}

}

class DataOverflowException extends RuntimeException{
	
	DataOverflowException(String str){
		super(str);
	}
}

class DataUnderflowException extends RuntimeException{

	DataUnderflowException(String str){
		
		super(str);
	
	}
}


