//If number divisible by 3 print "fizz"
//If number divisible by 5 print "buzz"
//if divisible by both then print "Both"
//If not divisible by "Both"
//
//HERE ONE SHOULD CONSIDER THE SEQUENCE OF CONDITIONS PASSED IN if-else-if.
//here if the first condition is placed at 3rd if's condition then the number will be considered ONLY DIVISIBLE BY 3 
//and the control will be out of ladder,but we want the output fizz-buzz,but we will get fizz only.

class Program3{
	public static void main(String[] args){
	
        int num=15;
	if((num%3==0)&&(num%5==0)){
	    System.out.println("fizz-buzz");
	}
	else if(num%3==0){
		System.out.println("fizz");
	}
	else if(num%5==0){
		System.out.println("buzz");
	}
	else{
	System.out.println("Not divisible");
	}	
	
	
	}	


}
