import java.util.concurrent.*;
import java.util.*;

class demo{
	public static void main(String args[]){
	
		BlockingQueue bq = new ArrayBlockingQueue(3);

		Producer producer = new Producer(bq);
		Consumer consumer  = new Consumer(bq);

		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);

		producerThread.start();
		consumerThread.start();
	}
}

class Producer implements Runnable{

	BlockingQueue bq;

	Producer(BlockingQueue bq){
		this.bq=bq;
	}

	public void run(){
	
		for(int i=1;i<=10;i++){
		
			try{
			
				bq.put(i);
				System.out.println("Producer "+i);
			}
			catch(Exception ie){
			
			}

			try{
			
				Thread.sleep(3000);
			}
			catch(Exception ie){
			
			
			}
		}
	}
}


class Consumer implements Runnable{

	BlockingQueue bq;

	Consumer(BlockingQueue bq){
		this.bq=bq;
	}

	public void run(){
	
		for(int i=1;i<=10;i++){
		
			try{
			
				bq.take();
				System.out.println("Consumer "+i);
			}
			catch(Exception ie){
			
			}

			try{
			
				Thread.sleep(7000);
			}
			catch(Exception ie){
			
			
			}
		}
	}

}
