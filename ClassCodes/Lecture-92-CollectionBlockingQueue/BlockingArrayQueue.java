import java.util.concurrent.*;

class demo{

	public static void main(String args[])throws InterruptedException{
	
		BlockingQueue bq = new ArrayBlockingQueue(3);

		bq.put(10);
		bq.put(20);
		bq.put(30);

		System.out.println(bq);

		bq.put(40);

	}
}

//here the 40 waits for entring the bq but as size is fixed hence it waits for empty spot in bq but as there is no call to the take method the element is not removed and location is not allowed 40 hence the put waits undefinitely
