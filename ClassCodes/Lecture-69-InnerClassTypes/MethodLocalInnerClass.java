class client{
	
	public static void main(String args[]){
	
		Outer obj = new Outer();
		obj.m1();
	
	}


}


class Outer{
	
	void m1(){
	
		System.out.println("In m1 Outer");

		class Inner{
		
			System.out.println("In Inner class m1");
		
		}

		Inner obj = new Inner();
		obj.m1();

		//object should be created in this class only otherwise there no option for calling 
		//method

	}

}

