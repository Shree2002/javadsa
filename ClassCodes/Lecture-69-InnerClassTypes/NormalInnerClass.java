
class client{
	
	public static void main(String args[]){
	
		Outer.Inner obj = new Outer().new Inner();
		obj.fun();
	
	}

}

class Outer{
	
	class Inner{
	
		void fun(){
			
			System.out.println("Fun in m1");
		
		}

	}

}
//for accessing the methods in inner class the object should be created in the above way
