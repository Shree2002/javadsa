class client{
	
	public static void main(String args[]){
	
		Outer obj = new Outer();
		obj.m1().m1();

		
	}

}

class Outer{

	Object m1(){
	
		System.out.println("m1 outer");

		class Inner{
			
			void m1(){
			
				System.out.println("m1 Inner");

			}

		}
		new Inner().m1();
		return new Inner();
	}

}
