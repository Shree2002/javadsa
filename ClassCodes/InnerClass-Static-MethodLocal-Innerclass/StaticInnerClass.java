class client{

	public static void main(String args[]){
	
		//StaticInnerDemo obj = new StaticInnerDemo();
		//StaticInnerDemo.InnerDemo obj1 = new StaticInnerDemo.InnerDemo();
		
		}
}

class StaticInnerDemo{

	void m1(){
		System.out.println("In m1 Outer");
	}
	
	static class InnerDemo{
	
		void m2(){
		
			System.out.println("In m2 inner");
		}
		
	
	} 

}
