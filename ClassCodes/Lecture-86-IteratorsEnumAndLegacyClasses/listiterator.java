import java.util.*;

class ArrayListDemo{

	public static void main(String args[]){
	

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(11);
		al.add(12);
		al.add(13);
		al.add(14);
		al.add(15);

		ListIterator itr = al.listIterator();

		while(itr.hasNext()){
		
			System.out.println(itr.next());
		}

		while(itr.hasPrevious()){
		
			System.out.println(itr.previous());
		}


	}
}
