import java.util.*;

class VectorDemo{

	public static void main(String args[]){
	
		Vector v = new Vector(5,7);	

		v.addElement(12);
		v.addElement(12);
		v.addElement(12);
		v.addElement(12);
		v.addElement(12);

		System.out.println(v.capacity());
		v.addElement(12);
		System.out.println(v.capacity());

		Enumeration cursor = v.elements();

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}
		
		System.out.println(cursor.getClass().getName());
		System.out.println(cursor.getClass());
	
	}
}
