import java.util.*;

class StackDemo{

	public static void main(String args[]){
	
		Stack st = new Stack();
		
		st.push(10);
		st.push(11);
		st.push(12);
		st.push(13);
		st.push(14);

		System.out.println(st);//print according to insertion order
				       //as per property of list

		System.out.println(st.peek());
		
		while(!st.isEmpty()){
			
			System.out.println(st.pop());

		}


		

	}
}
