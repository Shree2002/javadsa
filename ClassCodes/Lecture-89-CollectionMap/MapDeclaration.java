import java.util.*;

class Demo{


	public static void main(String args[]){
	
		HashMap hm = new HashMap();
		
		hm.put("Shree","Oracle");
		hm.put("Kartik","Amazon");
		hm.put("Harshal","Veritas");

		//updates in the hash map are done by out method with same key
		//and desired value
		hm.put("Shree","Microsoft");

		System.out.println(hm);

		HashSet hs = new HashSet();
		
		hs.add("Shree");
		hs.add("Kartik");
		hs.add("Harshal");

		System.out.println(hs);
	//hashset and hash map stores the elements in the same manner as hashset imolmentation is hashmap backed

		//get method
		System.out.println(hm.get("Shree"));

		//keyset method is used to covert the map into set
		//its return type is set
		System.out.println(hm.keySet());

		//Collection<V>values() method is used to represent all values in the map in the form of collection 
		System.out.println(hm.values());

		//this method returns set of entries 
		System.out.println(hm.entrySet());  
	}

}
