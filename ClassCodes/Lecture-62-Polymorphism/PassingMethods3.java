class Client {
	public static void main(String args[]){
	Parent obj = new Child();
	obj.fun();
	}
}

class Parent{
	void fun(){
	System.out.println("In parent fun");
	}
}

class Child extends Parent{
	void fun(int x){
	System.out.println("In Child method");
	}
}
//the method fun() is found by the compiler in the reference class (Parent) and the object child can access the method from parent and at runtime it can run the fun() method 
