class Client{
	public static void main(String args[]){
	Parent obj =  new Child();
	obj.fun();
	}
}
class Parent{
	Parent(int x){
	System.out.println("Constructor in Parent class");
	}

	void fun(){
	System.out.println("Fun in Parent");
	}
}

class Child extends Parent{
 	 Child(){
  	System.out.println("In Child constuctor");
  	}

	void fun(){
	System.out.println("Fun in Child");
	}
}
//here the compiler searches for the fun method with no arguments in parent (method table with method si//gnatures of methods in parent) as the reference of parent class is given 
//but it cannot find the method thats whay this program gives error 
