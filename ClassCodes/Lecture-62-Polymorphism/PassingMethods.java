class Client{
	public static void main(String args[]){
	Parent obj = new Child();
	obj.fun();
	}
}

class Parent{
	Parent(){
	System.out.println("In Parent constructor");
	}

	void fun(int x){
	System.out.println("Fun in parent class");
	}
}
class Child extends Parent{
	Child(){
	System.out.println("In Child class");
	}

	void fun(){
	System.out.println("Fun in Child class");
	}
}
// the calling object of the class Child on reference of Parent class
//the type of overriding like this is only possible if fun() method is TOTALLY SAME IN BOTH CLASSES


