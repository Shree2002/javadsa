import java.util.Scanner;

class Demo{
	
	public static void main(String args[]){
		
		int x = new Scanner(System.in).nextInt();


		try{
		
			System.out.println(10/x);
			if(x==0){
				
				throw new ArithmeticException("Arithmetic Exception caught");

			}


		}
		catch(ArithmeticException obj){
			System.out.println("Caught Arithmetic exception");
		}

	}


}

//throw keyword is used for throwing the customised exceptions in java
//throw keyword is used in the method while the throws is declared at the method declaration 
