
class Child extends Parent{
        void call(){
	System.out.println(super.x);
	super.access();
	}
	public static void main(String args[]){
	Parent obj = new Parent();
	Child obj1 = new Child();
	obj1.call();
	}
}
class Parent{

	int x = 10;
	static int y = 20;

	Parent(){
	System.out.println("The Parent internship");
	}

	Parent(int x){
	System.out.println("The constructor with parameter x");
	}

	void access(){
	System.out.println("The access method");
	}	
}


