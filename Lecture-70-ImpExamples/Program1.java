class Demo{

	Demo(parent p){
	
		System.out.println("In constructor parent");
		p.m1();
	
	}

	Demo(child c){
	
		System.out.println("In child constructor");
		c.m1();
	
	}

	public static void main(String args[]){
	
		Demo obj1 = new Demo(new parent());
		Demo obj2 = new Demo(new child());

	}

}
class parent{
	
	int x = 10;

	void m1(){
	
		System.out.println("In parent class ");
	
	}

}

class child extends parent{
	
	int a = 20;

	void m1(){
	
		System.out.println("In child class");

	}

}

