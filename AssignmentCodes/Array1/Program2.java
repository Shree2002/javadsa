import java.io.*;
class Program2 {
   public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    System.out.println("Enter the number of elements in array");
    int size = Integer.parseInt(br.readLine());
      int arr[] = new int[size]; 
      int product = 1;
    System.out.println("Enter the elements in the array : ");
    for(int i=0;i<size;i++){
      arr[i] = Integer.parseInt(br.readLine());
      if(arr[i]%2==0){
         product *= arr[i];
      }
    }
    System.out.println("Product of even elements is : "+product);
   } 
}
