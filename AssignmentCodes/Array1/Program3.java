import java.io.*;
class Program3 {
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    System.out.println("Enter the number of elements in array");
    int size = Integer.parseInt(br.readLine());
      int arr[] = new int[size]; 
      int product = 1;
    System.out.println("Enter the elements in the array : ");
    for (int i = 0; i < size; i++) {
        arr[i]=Integer.parseInt(br.readLine());
        if(i%2==1){
            product *= arr[i];
        }
    }
    System.out.println("The product of the odd indexed elements : "+product);
    }
}
