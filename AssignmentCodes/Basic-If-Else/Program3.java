import java.io.*;

class Program3 {
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the number ");
        int num = Integer.parseInt(br.readLine());

        if(num > 0){
            System.out.println("The number is positive");
        }
        else if(num == 0){
            System.out.println("The number is neither positive nor negative");
        }
        else{
            System.out.println("The number is negative");
        }

    }
}
