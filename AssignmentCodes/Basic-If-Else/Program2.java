import java.io.*;

class Program2 {
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the number ");
        int num = Integer.parseInt(br.readLine());

        if(num > 10){
            System.out.println("The number is greater than 10");
        }
        else if(num == 10){
            System.out.println("The number is equal to 10");
        }
        else{
            System.out.println("The number is less than 10");
        }

    }
}
