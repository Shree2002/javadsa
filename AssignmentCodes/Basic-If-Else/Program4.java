import java.io.*;

class Program4 {
    public static void main(String[] args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the number ");
        int num = Integer.parseInt(br.readLine());
        
        if(num == 1){
            System.out.println("One");
        }
        else if(num == 2){
            System.out.println("Two");
        }
        else if(num == 3){
            System.out.println("Three");
        }
        else if(num == 4){
            System.out.println("Four");
        }
        else if(num == 5){
            System.out.println("Five");
        }
        else if(num < 0){
            System.out.println("The number is negative");
        }
        else{
            System.out.println("The number is greater than 5");
        }

    }
}
