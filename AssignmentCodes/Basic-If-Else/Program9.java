import java.io.*;

class Program9{
    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Enter the first edge length ");
        int num1 = Integer.parseInt(br.readLine());

        System.out.println("Enter the second edge length ");
        int num2 = Integer.parseInt(br.readLine());

        System.out.println("Enter the hypotenuse ");
        int num3 = Integer.parseInt(br.readLine());

        if((num1*num1)+(num2*num2)==(num3*num3)){
            System.out.println("The given numbers are pythagorian triplet");
        }
        else{
            System.out.println("The given numbers are not  pythagorian triplet");

        }
        
    }
}