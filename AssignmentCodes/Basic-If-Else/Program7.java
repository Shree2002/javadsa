import java.io.*;

class Program7 {
    public static void main(String[] args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Enter the cost price  ");
        int cost = Integer.parseInt(br.readLine());

        System.out.println("Enter the selling price  ");
        int sell = Integer.parseInt(br.readLine());
        
        if(sell > cost){
            System.out.println("The profit is "+(sell-cost));
        }
        else if(sell==cost){
            System.out.println("No profit no loss");
        }
        else{
            System.out.println("The loss of "+ (cost - sell));
        }

        

    }
}
