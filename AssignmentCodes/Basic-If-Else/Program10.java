import java.io.*;

class Program10 {
    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Enter the budget : ");
        int num1 = Integer.parseInt(br.readLine());

        if(num1 > 1000){
            System.out.println(" Destination: Lonavala");
        }
        else if(1000>num1 && num1>=500){
            System.out.println("Destination: Lavasa");
        }
        else if(num1<500 && num1>=100){
            System.out.println("Destination: Narhe");
        }
        else{
            System.out.println("Destination : Home");
        }
    }
}
