import java.io.*;

class Program8 {
    public static void main(String[] args) throws IOException{
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of the array:");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter the elements in the array:");

        for(int i=0;i<size;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }

        for (int i = 0; i < arr.length; i++) {
            int num=arr[i];
            int cnt=0;
            int sum=0;

            while(num>0){
                cnt++;
                num /=10;
            }

            num=arr[i];

            while(num > 0){
                int mult=1;
                int rem = num%10;

                for(int j=1;j<=cnt;j++){
                    mult *= rem;
                }
                sum += mult;
                num /=10;
            }

            if(arr[i] == sum){
                System.out.println("Armstrong number "+arr[i]+" found at index : "+i);
            }

        }        

    }
}
