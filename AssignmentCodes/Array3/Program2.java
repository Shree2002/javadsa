import java.io.*;
class Program2 {
    public static void main(String[] args) throws IOException{
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of the array:");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter the elements in the array:");

        for(int i=0;i<size;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }

        for(int i=0;i<size;i++){
            int num =arr[i];
            int rev=0;
            while(num>0){
                rev = rev*10 + num%10;
                num /= 10;
            }

            System.out.println("The reversed number of "+arr[i]+" is "+rev);
        }

    }
    
}
