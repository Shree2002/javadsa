import java.io.*;

class Program1{
    public static void main(String[] args)throws IOException {
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of the array:");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i=0;i<size;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }

        System.out.println("The number of digits in each element in the array are as follows : ");

        for(int i=0;i<size;i++){
            int temp=arr[i];
            int count=0;
            while(temp>0){
                count++;
                temp /= 10;
            }
            System.out.println("the element at "+ i+"th index has "+count+" digits");
            
        }

    }
}