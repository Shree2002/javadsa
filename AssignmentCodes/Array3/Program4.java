import java.io.*;

class Program4 {
    public static void main(String[] args)throws IOException {
        
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of the array:");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter the elements in the array:");

        for(int i=0;i<size;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }

        for(int i=0;i<size;i++){
            int count=0;
            for(int j=1;j*j<arr[i];j++){
                if(arr[i]%j==0){
                    count++;
                }
            }
            if(count==1){
                System.out.println("Prime number "+arr[i]+" found at "+i+" index");
            }
        }


    }
    
}
