import java.io.*;

class Program9 {

    public static void main(String[] args) throws IOException{
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of the array:");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter the elements in the array:");

        for(int i=0;i<size;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }

        int temp = 0;
        if(size>2){

            for(int i=0;i<arr.length;i++){
                for(int j=i+1;j<arr.length;j++){
                    if(arr[i]>arr[j]){
                        temp=arr[i];
                        arr[i]=arr[j];
                        arr[j]=temp;
                    }
                }
            }
            
        }
        else{
            System.out.println("The size array is "+size);
        }

        System.out.println("the second largest element is "+ arr[arr.length-2]);


    }
    
}
