class main{

	static int[] twoSum(int arr[],int target){
	
		   int arr1[]=new int[2];

      for(int i=0;i<arr.length;i++){
          for(int j=i+1;j<arr.length;j++){

                if(target == arr[i]+arr[j]){
                    arr1[0]=i;
                    arr1[1]=j;
                    return arr1;
                }

          }
      }

        return arr1;
	}

	public static void main(String arg[]){
	
		int sample[] = new int[]{1,5,4,8,9,0};
		int ans[] = twoSum(sample,13);

		for(int i=0;i<ans.length;i++){
		
			System.out.println(ans[i]);
		}
	}

}
