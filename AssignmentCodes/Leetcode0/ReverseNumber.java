class main{


	static int reverseNumber(int x){
		int n=x;
        long reverse = 0;

        while(n!=0){

            reverse = reverse*10 + n%10;
            n /= 10;
        }

        if(reverse > Integer.MAX_VALUE || reverse < Integer.MIN_VALUE){
            return 0;
        }
        else{
            return (int)reverse;
        }

	
	}

	public static void main(String args[]){
		
		System.out.println(reverseNumber(13254));		
		


	}
}
