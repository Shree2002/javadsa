//time to equality
//an array with integer n ,in one second you can increase the value of element by 1
//find minimum ,time in seconds to make all elements equal

import java.util.Scanner;
class main{

	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of elements in array");
		int n = sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter the elements in array ");
		int max = Integer.MIN_VALUE;
		for(int i=0;i<n;i++){
		
			arr[i] = sc.nextInt();
			if(arr[i] > max)
				max=arr[i];
		}

		int time=0;

		for(int i=0;i<n;i++){
		
			time += max - arr[i];
		}

		System.out.println("Time is "+time);


	}

}
