//sum of maximum and minimum element in an array

class main{

	public static void main(String args[]){
	
		int arr[] = {-2,-1,-4,5,3};
		int n=arr.length;

		int max=Integer.MIN_VALUE;
		int min=Integer.MAX_VALUE;

		int start=0;
		int end=n-1;

		for(int i=0;i<n;i++){
		
			if(min > arr[i]){
				min=arr[i];
			}
		}

		for(int i=0;i<n;i++){
		
			if(max < arr[i]){
			
				max=arr[i];
			}
		}

		System.out.println(max+min);
	
	}
}
