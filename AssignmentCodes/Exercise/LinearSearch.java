//linear search - return number of occurances of element

import java.util.*;

class main{

	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of elements in array:");
		int n = sc.nextInt();

		int arr[] = new int[n];

		for(int i =0;i<n;i++)
			arr[i]=sc.nextInt();

		int cnt=0;

		System.out.println("Enter the number you want to find");
		int find = sc.nextInt();

		for(int i=0;i<arr.length;i++){
			if(arr[i] == find)
				cnt++;			
			
		}

		System.out.println("The count of number is "+cnt);
	}
}
