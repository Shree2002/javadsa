//In place prefix

import java.util.Scanner;
class main{

	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of elements in array");
		int n = sc.nextInt();

		int arr[] = new int[n];

		for(int i=0;i<n;i++){
		
			arr[i] = sc.nextInt();
		}

		for(int i=1;i<n;i++){
		
			arr[i] = arr[i]+arr[i-1];
		}


		for(int i=0;i<n;i++){
		
			System.out.println(arr[i]);
		}
	}
}
