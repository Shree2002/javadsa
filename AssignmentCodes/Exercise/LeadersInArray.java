//leader in array is the element having all right sided elements smaller than itself

import java.util.*;
class main{

	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of elements in an array");
		int n= sc.nextInt();

		int arr[] = new int[n];

		for(int i=0;i<n;i++){
		
			arr[i] = sc.nextInt();
		}
		
		int maxi=Integer.MIN_VALUE;
        	ArrayList<Integer>ans=new ArrayList<>();
       		for(int i=n-1;i>=0;i--)
       		{
           		if(arr[i]>=maxi)
           		{	
           			ans.add(arr[i]);
           			maxi=arr[i];
           		}

       		}
       		Collections.reverse(ans);

       		for(Integer i:ans){
       
	       	System.out.println(i);
 	      }


	}
}
