//You are given an array if n integers
//a 2d matrix of Mx2 is given 
//from ranges given in matrix find the sum of elements in array

class main{

	public static void main(String args[]){
	
		int arr[]={1,2,3,4,5};
		int m[][]={{0,3},{1,2}};

		for(int i=0;i<m.length;i++){
		
			int start = m[i][0];
			int end = m[i][1];
			
			int sum=0;	

			for(int j=start;j<=end;j++){
					
				sum += arr[j];

			}
			System.out.println("The sum of elements from " +start+ "to "+end+ " is "+ sum);
		}
	}
}
