//construct an product array which consists the elements where each element is division of product of all elements in the array and arr[i] element

import java.util.Scanner;
class main{

	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of elements in array");
		int n = sc.nextInt();

		int arr[]=new int[n];
		int product1[] = new int[n];
		int product=1;
		for(int i=0;i<n;i++){
		
			arr[i]=sc.nextInt();
			product *= arr[i];
		}

		for(int i=0;i<n;i++){
		
			product1[i] = product/arr[i];
		}

		for(int i=0;i<n;i++){
		
			System.out.println(product1[i]);
		}
	}
}
