
import java.io.*;

class Program6 {
   public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter the elements in the array:");
    int size = Integer.parseInt(br.readLine());

    int arr[] = new int[size];
    int max =0;
    for (int i = 0; i < arr.length; i++) {
        arr[i] = Integer.parseInt(br.readLine());

        if(max<arr[i])max=arr[i];

    }

    System.out.println("The maximum element in the array is :"+max);
   }

}