import java.io.*;

class Program1{
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the elements in the array:");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];
        int count=0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(br.readLine());
            count += arr[i];
        }
        System.out.println("the of all elements is : "+count);


    }
}