
import java.io.*;

class Program2 {
   public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter the elements in the array:");
    int size = Integer.parseInt(br.readLine());

    int arr[] = new int[size];
    int even=0,odd=0;
    for (int i = 0; i < arr.length; i++) {
        arr[i] = Integer.parseInt(br.readLine());
        if(arr[i]%2==0)even++;
        if(arr[i]%2==1)odd++;
    }

    System.out.println("The count of even number is " + even);
    System.out.println("The count of odd number is " + odd);

   }
}
