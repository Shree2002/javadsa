import java.io.*;

class Program10 {
    public static void main(String[] args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of the array : ");
        int size = Integer.parseInt(br.readLine());
        
        int arr[] = new int [size];

        System.out.println("Enter the elements in the array:");
        for(int i=0;i<size;i++){
        arr[i] = Integer.parseInt(br.readLine());    
        }

        System.out.println("The numbers with sum of the digits even are:");

        for(int i=0;i<size;i++){
            
            int num = arr[i];
            int sum=0;

            while(num>0){
                sum += num%10;
                num /= 10;
            }
            if(sum%2==0){
                System.out.println(arr[i]);
            }
            sum=0;
            }

    }
}
