
import java.io.*;

class Program5 {
   public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter the elements in the array:");
    int size = Integer.parseInt(br.readLine());

    int arr[] = new int[size];
    int min =0;
    for (int i = 0; i < arr.length; i++) {
        arr[i] = Integer.parseInt(br.readLine());

        if(min>arr[i])min=arr[i];

    }

    System.out.println("The minimum element in the array is :"+min);



   }

}