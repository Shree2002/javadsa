import java.io.*;

class Program9 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the size of first array:");
        int arr1size = Integer.parseInt(br.readLine());

        int arr1[] = new int[arr1size];

        System.out.println("Enter the elements int the first array :");
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = Integer.parseInt(br.readLine());
        }


        System.out.println("Enter the size of second array:");
        int arr2size = Integer.parseInt(br.readLine());

        int arr2[] = new int[arr1size];

        System.out.println("Enter the elements int the second array :");
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = Integer.parseInt(br.readLine());
        }

        int arr3[] = new int [arr1size+arr2size];

        for(int i=0;i<arr1size;i++){
            arr3[i] = arr1[i];
        }

        for(int i=0;i<arr2size;i++){
            arr3[arr1size+i]=arr2[i];
        }

        System.out.println("Merged array is :");
        System.out.print("[");
        for(int i=0;i<arr3.length;i++){
            System.out.print(arr3[i]+",");
        }
        System.out.println("]");

    }
    
}
