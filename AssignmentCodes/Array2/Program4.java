import java.io.*;

class Program4 {
    
    public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter the elements in the array:");
    int size = Integer.parseInt(br.readLine());
    
    int arr[] = new int[size];

    for (int i = 0; i < arr.length; i++) {
        arr[i] = Integer.parseInt(br.readLine());
    }

    System.out.println("Enter the element to find  in the array:");
    int element = Integer.parseInt(br.readLine());

    for (int i = 0; i < arr.length; i++) {
        if(arr[i] == element){
            System.out.println("The element found at : "+i);
        }
    }


    }

}
