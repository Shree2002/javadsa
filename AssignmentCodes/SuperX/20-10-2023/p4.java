//write sum of all digits from given range

import java.util.*;

class demo{

	static int sum(int num){
		if(num==0)
			return 0;
		return (num*(num+1))/2;

	}

	public static void main(String args[]){
		
	Scanner sc  = new Scanner(System.in);

	int start = sc.nextInt();
	int end  = sc.nextInt();

	System.out.println(sum(end)-sum(start-1));

	}
}
