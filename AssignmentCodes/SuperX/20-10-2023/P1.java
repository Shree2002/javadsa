// A B C D
// # # # #
// A B C D
// # # # #

import java.util.*;
class demo{

	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);

		int number = sc.nextInt();

		for(int i=0;i<number;i++){
		
			if(i%2==0){
				char s = 'A';
				for(int j=0;j<number;j++){
				
					System.out.print(s++ + " ");
				}
				System.out.println();
			}
			else{
			
				for(int j=0;j<number;j++){
				
					System.out.print("#"+ " ");
				}
				System.out.println();
			}
		}
	}
}
