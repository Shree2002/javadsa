class demo{

	static int index(int num[]){
	
		int number=-1;

		for(int i=0;i<num.length;i++){
		
			if(i%10 == num[i]){
			
				if(number < i)
				       number=i;	
			}
		}

		return number;

	}

	public static void main(String args[]){
	
		int arr[] = {4,3,2,1};

		System.out.println(index(arr));
	}
}
