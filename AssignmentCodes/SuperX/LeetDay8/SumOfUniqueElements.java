import java.util.*;

class demo{


	static 	int SumOfUnique(int arr[]){
		
		Set<Integer>st=new HashSet<Integer>();

		for(int i=0;i<arr.length;i++){
		
			st.add(arr[i]);
		}

		int sum=0;

		for(Integer i:st){
		
			sum += i;
		}

		return sum;

	}

	public static void main(String args[]){
	
		int arr[] = {1,2,3,2};
		System.out.println(SumOfUnique(arr));
	}
}
