import java.util.*;

class main{

	public static void main(String name[]){
	
		Scanner sc = new Scanner(System.in);

		int start = sc.nextInt();
		int end = sc.nextInt();

		for(int i=start;i<=end;i++){
		
			System.out.println(fact(i));
		}
		
	
	}

	static int fact(int n){
	
		int fact = 1;

		for(int i=1;i<=n;i++){
		
			fact *= i;
		}

		return fact;
	}

}
