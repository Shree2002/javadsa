//strong number

import java.util.*;

class main{

	public static void main(String args[]){
	
		int num = new Scanner(System.in).nextInt();

		int num1 = num;
		int sum = 0;

	while(num>0){
	
		sum += fact(num%10);
		num /= 10;
	}

	if(sum==num1)
		System.out.println("Strong number");
	else
		System.out.println("Not a Strong number");


	}

	static int fact(int n){
		int fact=1;

		for(int i=1;i<=n;i++){
		
			fact *= i;
		}
		return fact;

	}
}
