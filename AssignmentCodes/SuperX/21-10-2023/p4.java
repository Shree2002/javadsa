// 1 3 5 7
// 2 4 6 8
// 9 11 13 15
// 10 12 14 16


import java.util.*;

class main{
	public static void main(String args[]){
		
		int row = new Scanner(System.in).nextInt();
		
		int num1=1;
		int num2=2;

		for(int i=0;i<row;i++){
		
			if(i%2==0){
				
				for(int j=0;j<4;j++){
				
					System.out.print(num1+ " ");
					num1=num1+2;
					
				}

				System.out.println();
			}
			else{
				
				for(int j=0;j<4;j++){
				
					System.out.print(num2 + " ");
					num2=num2+2;
					
				}
				System.out.println();

			}
		}

	}

}
